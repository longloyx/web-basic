class Circle {
  constructor(radius) {
    this.r = radius
  }
  getArea(){return Math.PI*(this.r ** 2)}
  
}

let q = new Circle(10);
console.log(q.getArea());
// console.log(q.getPerimeter());